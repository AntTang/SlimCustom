<?php
/**
 * @package     routes.php
 * @author      Jing <tangjing3321@gmail.com>
 * @link        http://www.slimphp.net
 * @version     1.0
 * @copyright   Copyright (c) SlimCustom.
 * @date        2017年4月5日
 */

namespace SlimCustom\bootstrap;

use \SlimCustom\Libs\App;

App::group('/admin', function () {
    $this->get('/index/{name}', \Demo\Controllers\Admin\Index::class . ':index')->add(\Demo\Middlewares\Admin\Index::class . ':index');
});
